# Jobcoin Implementation
This is an implementation of the Jobcoin project.
This implementation is built with ReactJs and Typescript.

# Project Structure
## Types
The data structures used by the components, containers and services
  * Transaction - time, from, to, amount
  * AccountStatus - balance and transactions list

## Components
The ui building blocks that emit events that are handled in the containers

  * balance.tsx - Displays User Balance
  * login.tsx - Login widget with login event
  * logo.tsx - Simple logo image

## Containers
Also known as 'views'
The components that string together multiple components and allow them to interface with services
  * AccountInfo.tsx - Displays user's balance and facilitates sends
  * Login.tsx - Uses the login component and JobCoinService to log a user in

## Services
Typescript classes that interface with the network via request, and return typecasted data from api calls
   * JobCoinService.ts - Service that interfaces with API

# User Journey
A user starts out on the login page, which is mounted on the root path, /

From the login page the user can enter a user name, and click the login button

If the username is incorrect, the user will receive an error message 'This is not a valid user'

If the username is correct the user will be brought to the account info page.

The account info page is mounted to the path /account

From the account info page, the user can see their current balance, a chart of current balance, and a widget to send coins

If the user sends coins, it should update the balance and the chart as well.

The user can logout from the account info page.

Once logged out, a user shouldn't be able to view the account info page without logging back in.



# Usage
```
npm install && npm start
```
