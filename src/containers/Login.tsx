import * as React from 'react';
import { Component } from 'react';
import {Logo} from '../components/logo';
import {Login} from '../components/login';
import {JobCoinService} from '../services/JobCoinService';
import { RouteComponentProps } from "react-router";

type State = {
  loggedInUser?: string;
  errorMsg?: string;
}

export class LoginContainer extends Component<RouteComponentProps<any>> {

  public state: State = {};

  constructor(props: any, context: any) {
    super(props, context);
    this.handleLogin = this.handleLogin.bind(this);
  }

  public async handleLogin(userId: string) {
    // check to see if user is valid
    let validUser = await JobCoinService.isValidUser(userId);
    if(validUser) {
      console.log(`Logged in as ${userId}`);
      this.setState(({
        loggedInUser: userId,
        redirect: true,
        errorMsg: ''
      }));
      this.props.history.push({pathname: '/account', state: {user: userId}});
    } else {
      this.setState(({errorMsg: 'This is not a valid user'}));
    }
  }

  render() {
    let styles = {
      logo: {
        gridArea : 'lg'
      },
      signIn : {
        gridArea: 'sb'
      }
    };

    let errorMsg = this.state.errorMsg ? <div className="errorMsg"> {this.state.errorMsg} </div> : null
    return (
      <div className='login-container'>
      <div style={styles.logo}>
      <Logo />
      </div>
      <div style={styles.signIn}>
      <Login onLogin={this.handleLogin} />
      {errorMsg}
      </div>
      </div>
    );
  }
}
