import * as React from 'react';
import { Component } from 'react';
import { Chart } from 'chart.js';
import { RouteComponentProps } from "react-router";
import { Logo } from "../components/logo";
import { Balance } from "../components/balance";
import { JobCoinService} from "../services/JobCoinService";
import { SendCoin } from "../components/sendCoin";
import { Transaction } from "../types/index";

type BalanceType = {
  amount: number;
  date: Date;
}
type State = {
  user?: string;
  balance?: string;
  transactions?: Array<Transaction>;
  balanceOverTime?: Array<BalanceType>;
};

export class AccountInfoContainer extends Component<RouteComponentProps<any>> {

  public state: State = {
    user: '',
    balance: '0',
    transactions: [],
    balanceOverTime: []
  };

  constructor(props: any, context: any) {
    super(props, context);
    this.signOut = this.signOut.bind(this);
    this.handleSend = this.handleSend.bind(this);
  }

  public componentDidMount() {
    let goHome = () => {
      this.props.history.push('/');
      this.setState(({redirect: true}));
    }
    if(this.props.location && this.props.location.state) {
      let user = this.props.location.state.user;
      if(user == null) {
        console.log('Redirecting back home, no user');
        goHome();
      } else {
        this.initState(user);
      }
    } else {
      goHome();
    }
  }

  public async initState(user: string){
    let acctInfo = await JobCoinService.getAddressInfo(user);
    const {balance, transactions} = acctInfo;
    this.setState(({user, balance, transactions}));
    let balanceOverTime = this.initBalanceOverTime(transactions);
    this.initChart(balanceOverTime);
  }

  public initBalanceOverTime(transactions: Array<Transaction>) {
    let totalBalance = 0;
    if(transactions != null) {
      let timeSeries = transactions.map((tx: Transaction) => {
        let balance= {} as BalanceType;
        if(tx.timestamp) {
          balance.date = new Date(tx.timestamp);
        }
        balance.amount = this.getNewBalanceFromTx(totalBalance, tx);
        totalBalance = balance.amount;
        return balance;
      });
      this.setState(({balanceOverTime: timeSeries}));
      return timeSeries;
    }
    console.log('No transactions!');
    return [];
  }

  public initChart(balanceSeries: Array<BalanceType>) {
    var ctx = "depositChart";
    let data = balanceSeries.map((elem) => ({x: elem.date,  y:elem.amount}));
    console.log(data);
    if(ctx != null) {
      var chart =  new Chart(ctx, {
        type: 'line',
        data: {
          datasets: [{
            label: 'Balance Over Time',
            data,
            backgroundColor: [
              'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
              'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            xAxes: [{
              type: 'time',
              time: {
                displayFormats: {
                  'millisecond': 'MMM DD',
                  'second': 'MMM DD',
                  'minute': 'MMM DD',
                  'hour': 'MMM DD',
                  'day': 'MMM DD',
                  'week': 'MMM DD',
                  'month': 'MMM DD',
                  'quarter': 'MMM DD',
                  'year': 'MMM DD',
                }
              }
            }],
          },
        }

      });
      return chart;
    }
    return null;
  }

  public getNewBalanceFromTx(balance: number, tx: Transaction) {
    let newBalance = balance;
    let txAmt = Number(tx.amount);
    if(tx.fromAddress == this.state.user && tx.amount) {
      newBalance =  balance - txAmt;
    }
    if(tx.toAddress == this.state.user && tx.amount) {
      newBalance = balance + txAmt;
    }
    return newBalance;
  }


  public signOut() {
    this.props.history.replace('/', {});
  }

  public handleSend(tx: Transaction) {
    JobCoinService.sendCoin(tx).then(() => {
      this.processTransaction(tx);
    });
  }

  public processTransaction(tx: Transaction) {
    let currentBalance = Number(this.state.balance);
    let balance = currentBalance - Number(tx.amount);

    let transactions = this.state.transactions || [];
    transactions.push(tx);

    let balanceOverTime = this.state.balanceOverTime || [];
    let newBalance = {} as BalanceType;
    newBalance.date = new Date();
    newBalance.amount = this.getNewBalanceFromTx(currentBalance, tx);
    balanceOverTime.push(newBalance);
    this.initChart(balanceOverTime);
    this.setState(({balance, transactions, balanceOverTime}));
  }


  render() {

    let styles = {
      logo: {
        height: '24px',
        margin: '5px'
      },
      menu :{
        gridArea: 'm'
      },
      balance: {
        gridArea: 'b'
      },
      send: {
        gridArea: 's'
      },
      graph: {
        gridArea: 'g'
      },
      leftMenuItems : {
        display: 'inherit',
        marginRight: 0,
        marginLeft: 'auto'
      },
      canvas: {
        height: '100%',
        width: '100%'
      }
    }
    return (
      <div className="account-info-container">
      <div style={styles.menu} className="ui menu">
      <div className="item">
      <Logo style={styles.logo} className="ui" height={styles.logo.height}/>
      Jobcoin Sender
      </div>
      <div style={styles.leftMenuItems}>
      <div className="item">{this.state.user}</div>
      <a className="item" onClick={this.signOut}>Sign Out</a>
      </div>
      </div>
      <div style={styles.balance} >
      <Balance balance={this.state.balance}/>
      </div>
      <div style={styles.send} >
      <SendCoin onSend={this.handleSend} fromAddress={this.state.user}/>
      </div>
      <div style={styles.graph} >
      <canvas style={styles.canvas} id="depositChart"></canvas>
      </div>
      </div>
    );
  }
}
