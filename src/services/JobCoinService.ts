import * as request from 'request-promise';
import * as Bluebird from 'bluebird';
import { Transaction, AccountStatus } from "../types/index";

export type RequestResp<T> = {
  statusCode: number;
  body: T;
}

export type RequestPromise<T> = Bluebird<T>;


export class JobCoinService {

  public static getAddressInfo(address: string): RequestPromise<AccountStatus> {
    return request(`http://jobcoin.gemini.com/resistant/api/addresses/${address}`)
    .then((resp) => JSON.parse(resp));
  }

  public static isValidUser(address: string) {
    return JobCoinService.getAddressInfo(address).then((resp) => {
      let hasBody = resp != null;
      let hasTransactions = hasBody && resp.transactions.length > 0;
      return hasTransactions;
    }).catch((e) => false);
  }


  public static sendCoin(tx: Transaction) {
    let reqConf = {
      url: `http://jobcoin.gemini.com/resistant/api/transactions`,
      form: tx
    };
    return request.post(reqConf);
  }
}
