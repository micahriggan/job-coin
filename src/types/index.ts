export type Transaction = {
  timestamp?: Date;
  fromAddress?: string;
  toAddress?: string;
  amount?: string;
};

export type AccountStatus = {
  balance: string;
  transactions: Array<Transaction>;
}


