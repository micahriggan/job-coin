import * as React from 'react';
import './App.css';
import {LoginContainer} from './containers/Login';
import {Router, Route, Switch} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory'
import { AccountInfoContainer } from './containers/AccountInfo';

const customHistory = createBrowserHistory();

class App extends React.Component {
  render() {
    return (
      <div className="App">
      <Router history={customHistory}>
      <Switch>
      <Route path="/account" component={AccountInfoContainer}/>
      <Route path="/" component={LoginContainer}/>
      </Switch>
      </Router>
      </div>
    );
  }
}

export default App;
