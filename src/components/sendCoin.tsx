import * as React from 'react';
import { Component } from 'react';
import { Transaction } from "../types/index";

// Typings for the component props
type Props = {
  amount?: string;
  fromAddress?: string;
  onSend: (tx: Transaction) => void
};

type State = {
  amount?: string;
  toAddress?: string;
}

export class SendCoin extends Component {
  public props : Props;
  public state: State = {};

  constructor(props: Props) {
    super(props);
    this.updateAmount = this.updateAmount.bind(this);
    this.emitSend = this.emitSend.bind(this);
    this.updateTo = this.updateTo.bind(this);
    this.updateAmount = this.updateAmount.bind(this);
  }

  public emitSend() {
    let tx = { amount: this.state.amount, toAddress: this.state.toAddress, fromAddress: this.props.fromAddress};
    this.props.onSend(tx);
  }


  public updateTo(event: React.FormEvent<HTMLInputElement> ) {
    console.log(event.currentTarget.value);
    let toAddress = event.currentTarget.value;
    this.setState(({toAddress}));
  }



  public updateAmount(event: React.FormEvent<HTMLInputElement> ) {
    console.log(event.currentTarget.value);
    let amount = event.currentTarget.value;
    this.setState(({amount}));
  }

  public render() {
    let styles = {
      sendCard: {
        width: '100%',
        margin: '15px',
        padding: '35px'
      },
      input: {
        marginTop: '20px',
        marginBottom: '20px'
      }
    }
    return (
      <div className="ui card" style={styles.sendCard}>
      <h3>Send Jobcoin</h3>
      <div className="ui divider"></div>
      <div style={styles.input}  className="ui input">
      <input type="text" placeholder="Destination Address" onChange={this.updateTo} />
      </div>
      <div style={styles.input}  className="ui input">
      <input type="text" placeholder="Amount to Send" onChange={this.updateAmount} />
      </div>
      <button className="ui primary button" onClick={this.emitSend} >"Send Jobcoins"</button>
      </div>
    );
  }
}
