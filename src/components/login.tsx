import * as React from 'react';
import { Component } from 'react';

// Typings for the component props
type Props = {
  userId?: string;
  onLogin: (userId: string) => void
};

export class Login extends Component {
  public props : Props;
  public state: {userId: string};

  constructor(props: Props) {
    super(props);
    this.updateUserId = this.updateUserId.bind(this);
    this.emitLogin = this.emitLogin.bind(this);
  }

  public emitLogin() {
    this.props.onLogin(this.state.userId);
  }

  public updateUserId(event: React.FormEvent<HTMLInputElement> ) {
    console.log(event.currentTarget.value);
    let userId = event.currentTarget.value;
    this.setState(({userId}));
  }

  public render() {
    let styles = {
      loginCard: {
        width: '100%',
        margin: '15px',
        padding: '35px'
      },
      loginInput: {
        marginTop: '20px',
        marginBottom: '20px'
      }
    }
    return (
      <div className="ui card" style={styles.loginCard}>
      <h3>Welcome! Sign in with your Jobcoin address </h3>
      <div className="ui divider"></div>
      <div style={styles.loginInput}  className="ui input">
      <input type="text" placeholder="Jobcoin Address" onChange={this.updateUserId} />
      </div>
      <button className="ui primary button" onClick={this.emitLogin} >"Sign In"</button>
      </div>
    );
  }
}
