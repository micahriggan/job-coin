import * as React from 'react';
import { Component } from 'react';

// Typings for the component props
type Props = {
  balance?: string;
};

export class Balance extends Component {
  public props : Props;
  public state: {userId: string};

  constructor(props: Props) {
    super(props);
  }


  public render() {
    let styles = {
      balanceCard: {
        width: '100%',
        margin: '15px',
        padding: '35px'
      },
    };
    return (
      <div className="ui card" style={styles.balanceCard}>
      <h3>Jobcoin Balance</h3>
      <div className="ui divider"></div>
      <h4>{this.props.balance}</h4>
      </div>
    );
  }
}
